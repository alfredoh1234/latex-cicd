# latex-cicd

A template for rendering latex files with GitLab CICD/runners

The import stuff is in `.gitlab-ci.yml`.

You can use `pdflatex` or `xelatex`.

To build your own container, just use `docker-image/Dockerfile`.
